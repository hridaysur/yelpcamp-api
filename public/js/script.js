var notify;
function speech(message){
    var msg = new SpeechSynthesisUtterance();
    var voices = window.speechSynthesis.getVoices();
    msg.voice = voices[3]; // Note: some voices don't support altering params
    msg.voiceURI = 'native';
    msg.volume = 1; // 0 to 1
    msg.rate = 1; // 0.1 to 10
    msg.pitch = 2; //0 to 2
    msg.text = message;
    msg.lang = 'en-US';
    
    msg.onend = function(e) {
      console.log('Finished in ' + event.elapsedTime + ' seconds.');
    };
    
    speechSynthesis.speak(msg);
}
           
 
var campgrounds = function(){          
$(document).ready(function(){
  $.get("/campgrounds", function(data, status){
      //camp = JSON.stringify(data);
     // var jso = JSON.parse(camp);
          if(Notification.permission === "default"){
              alert("please allow notifications");
            } else {
                notify = new Notification("New Campground", {
                         body: data[1].name,
                         icon: data[1].image, 
                         tag : data[1]._id
                      });
                      speech("New Campground Added");
                  }
        //console.log("Data: " + camp);
                
        notify.onclick = function(){
                    window.location = '/campgrounds/' + this.tag; 
                }

        setTimeout(notify.close.bind(notify), 5000);
           //alert("Data: " + data + "\nStatus: " + status);
        });
});
};

campgrounds();

console.log(campgrounds);