module.exports = function(app) {
    var user = require('../controllers/userController');

    app.route('/signup')
    .post(user.create_a_user);
};  