module.exports = function(app) {
  var campGrounds = require('../controllers/campgroundController');

  // campground Routes
  
  app.route('/campgrounds')
    .get(campGrounds.list_all_campGrounds)
    .post(campGrounds.create_a_campGround);


  app.route('/campgrounds/:id')
    .get(campGrounds.read_a_campGround)
    .put(campGrounds.update_a_campGround)
    .delete(campGrounds.delete_a_campGround);
};