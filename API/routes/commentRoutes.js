module.exports = function(app) {
  var comment = require('../controllers/commentController');

  // comment Routes
  
  app.route('/comments')
    .get(comment.list_all_comments)
    .post(comment.create_a_comment);


  app.route('/comments/:id')
    .get(comment.read_a_comment)
    .put(comment.update_a_comment)
    .delete(comment.delete_a_comment);
};