'use strict';


var mongoose = require('mongoose'),
  Campground = mongoose.model('Campground'),
  Comment = mongoose.model('Comment');
  

exports.list_all_campGrounds = function(req, res) {
  Campground.find().populate('comments','text author1').exec()
  .then(camps => res.status(200).json(camps))
  .catch(err => res.status(404).json({
    Error:err
  }));
};


exports.create_a_campGround = function(req, res) {
  var name= req.body.name;
  var image = req.body.image;
  var description = req.body.description;
  var newCampgrounds= {name: name, image: image, description:description};

  Campground.create(newCampgrounds)
  .then(newcamps => res.status(200).json(newcamps))
  .catch(err => res.status(404).json(err))
  
  
};

exports.read_a_campGround = function(req, res) {
  Campground.findById(req.params.id).populate('comments','text author').exec()
  .then(foundCamp => res.status(200).json(foundCamp))
  .catch(err => res.status(404).json({
    Error:err
  }));
};

exports.update_a_campGround = function(req, res) {
  Campground.findOneAndUpdate({_id: req.params.id}, req.body, {new: true}, function(err, updateCamp) {
    if (err){
     console.log(err);   
    } else {
        res.json(updateCamp);
    }
  });
};

exports.delete_a_campGround = function(req, res) {
  Campground.remove({
    _id: req.params.id
  }, function(err, deleteCamp) {
    if (err){
     console.log(err);   
    } else {
    res.json({ message: 'Task successfully deleted' });
    }
  });
};

