'use strict';
var mongoose = require('mongoose'),
  User = mongoose.model('User');

var bcrypt = require('bcrypt');


  exports.create_a_user = function(req,res){
    bcrypt.hash(req.body.password, 10,(err, hash)=>{
        if(err){
            return res.status(500).json({
              error:err
            })
        } else {
            const User = new User({
                _id: new mongoose.Types.ObjectId(),
                email: req.body.email,
                password: hash
            });
            User.save()
            .then(result => {
                console.log(result);
                res.status(201).json({
                    message:"user is created"
                })
            })
            .catch(err => {
                console.log(err);
                res.status(500).json({
                    error:err
                })
            });
        }
    })
  }   
  
