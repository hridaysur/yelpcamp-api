'use strict';


var mongoose = require('mongoose'),
  Comment = mongoose.model('Comment');
  

exports.list_all_comments = function(req, res) {
  Comment.find({}, function(err, comment) {
    if (err){
        console.log(err);
    } else {
         res.status(200).json(comment);
    }
  });
};


exports.create_a_comment = function(req, res) {
  var new_comment = new Comment(req.body);
  new_comment.save()
  .then(newComment => res.json(newComment))
  .catch(
    err => res.status(404).json({
              Error: err
  }));
 
};

exports.read_a_comment = function(req, res) {
  Comment.findById(req.params.id, function(err, foundComment) {
    if (err){
        console.log(err);
    } else {
        res.json(foundComment);
    }
  });
};

exports.update_a_comment = function(req, res) {
  Comment.findOneAndUpdate({_id: req.params.id}, req.body, {new: true}, function(err, updateComment) {
    if (err){
     console.log(err);   
    } else {
        res.json(updateComment);
    }
  });
};

exports.delete_a_comment = function(req, res) {
  Comment.remove({
    _id: req.params.id
  }, function(err, deleteComment) {
    if (err){
     console.log(err);   
    } else {
    res.json({ message: 'Task successfully deleted' });
    }
  });
};

