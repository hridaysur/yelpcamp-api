'use strict';
var mongoose = require("mongoose");

// camp schemas setup

var commentSchema = new mongoose.Schema({
    text: String,
    author: String
});

module.exports = mongoose.model("Comment", commentSchema);
