var express = require("express"),
    app = express(),
    bodyParser = require("body-parser"),
    mongoose = require("mongoose"),
    Campground = require("./models/campground"),
    Comment = require("./models/comment"),
    User = require("./models/user"),
    seedDB = require("./seeds"),
    request = require("request")

mongoose.connect("mongodb://localhost/yelp_camp");

app.use(bodyParser.urlencoded({extended: true}));

app.set("view engine", "ejs");


seedDB();

app.get("/", function(req, res){
    res.render("landing");
})

// INDEX restfull routes
app.get("/campgrounds", function(req, res){
    //get all campgrounds from db
    request('http://localhost:4000/campgrounds', function (error, response, body) {
  console.log('error:', error); // Print the error if one occurred
  console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
  var value = JSON.parse(body);
  res.render("index", {campgrounds : value});
});
})

// create RESTfull routes
app.post("/campgrounds", function(req, res){

    request('http://localhost:4000/campgrounds', function(err,res,body){
        if(err){
            console.log(err);
        } else {
            res.redirect("/campgrounds");
        }
    })
       
    // creat a new campground and save it to DB
    // Campground.create(newCampgrounds, function(err, newlyCreated){
    //     if(err){
    //         console.log(err);
    //     } else {
    //          res.redirect("/campgrounds");
    //     }
    // });
    
})

//new RESTfull routes
    app.get("/campgrounds/new", function(req, res) {
    res.render("new.ejs");
    })

//show restfull routes
    app.get("/campgrounds/:id", function(req, res) {
    //find the campground with provided id
    request('http://localhost:4000/campgrounds/' + req.params.id, function (error, response, body) {
  console.log('error:', error); // Print the error if one occurred
  console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
  var value = JSON.parse(body);
  res.render("show",{campground:value});
 // Print the HTML for the Google homepage.
    });
   
})

app.listen(3000, function(){
    console.log("The yelp Camp server has started");
})
