var express = require('express'),
  app = express(),
  mongoose = require('mongoose'),
  campground = require('./API/models/campgroundModel'), //created model loading here
  comment = require('./API/models/commentModel'),
  user = require('./API/models/userModel'),
  bodyParser = require('body-parser'),
  request = require("request");
  
// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/yelp_camp_API'); 

app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const port = process.env.PORT || 4000;

app.use((req, res, next) => {
  res.header ('Access-Control-Allow-Origin', '*');
  res.header ('Access-Control-Allow-Headers', '*');
  if(req.method === 'OPTIONS'){
      res.header('Access-Control-Alloe-Method','PUT,PATCH,DELETE,POST,GET');
      return res.status(200).json({}); 
  };
  next();
});


var campgrounds_routes = require('./API/routes/campgroundRoutes'), //importing route
  comment_routes = require('./API/routes/commentRoutes'),
  user_routes = require('./API/routes/userRoutes');

campgrounds_routes(app); //register the route
comment_routes(app);
user_routes(app);


app.use(function(req, res) {
  res.status(404).send({url: req.originalUrl + ' not found'})
});


app.listen(port, function(){
    console.log("The yelp Camp server has started");
})